<div class="jumbotron text-center" style="margin-top:-20px; background-color: #daa520;  border-radius: 0">
	<h1 class="display-4">GOLD COFFEE HOUSE</h1>
	<p class="lead">Главная "изюминка" сети кофеен "Gold Coffee House" заключается в уникальности. Особый интерьер, творческие напитки, оригинальный подход - всё это помогает создать нашу непревзойдённость.<br>
	Мы постарались создать максимально уютную атмосферу в наших кофейнях, чтобы вы могли себя чувствовать как самый желанный гость.<br>
	У нас вы можете выбрать напиток на любой вкус. Не любите новшества? Тогда мы сможем предложить вам широкий ассортимент классического кофе. А если вам надоело однообразие и хочется чего-то нового, попробуйте один из множества наших авторских напитков.<br>
	Будем рады видеть вас в любой из наших кофеен!</p>
</div>
<div class="container">
	<div class="row row-cols-2">
		<div class="col"><img src="images/img1.jpg" class="img-fluid" style="margin-bottom: 30px"></div>
		<div class="col"><img src="images/img2.jpg" class="img-fluid"></div>
		<div class="col"><img src="images/img3.jpg" class="img-fluid"></div>
		<div class="col"><img src="images/img4.jpg" class="img-fluid"></div>
  	</div>
</div>
<div class="jumbotron text-center" style="margin: 20px 0 0 0; background-color: #daa520;  border-radius: 0">
</div>