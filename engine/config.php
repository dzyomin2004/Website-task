<?php
ob_start();
session_start();

$protocol = !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on' ? 'http' : 'https';
define('URL', ''. $protocol . '://'. $_SERVER['HTTP_HOST'] . '');


require_once('MySQL.php');
require_once('functions.php');

$user = isset($_SESSION['login'], $_SESSION['password']) ? authentication($_SESSION['login'], $_SESSION['password']) : 0;

switch ($user && isset($_GET['logout'])) {
    case 'logout':
        unset($_SESSION['login'], $_SESSION['password']);
        session_destroy();
        header('Location: ' . URL);
        break;
}