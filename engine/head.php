<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
<link rel="shortcut icon" type="image/png" href="<?php echo URL; ?>/favicon.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<?php echo URL; ?>/css/bootstrap 4.4.1.css" rel="stylesheet" type="text/css">
<script src="<?php echo URL; ?>/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo URL; ?>/js/popper.min.js"></script>
<script src="<?php echo URL; ?>/js/bootstrap-4.4.1.js"></script>
<script src="https://kit.fontawesome.com/b0bc056f5f.js" crossorigin="anonymous"></script>