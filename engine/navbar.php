<body style="padding-top: 70px; background-color: #0A0D21">
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark " style="background-color: #0A0D21">
	<a class="navbar-brand" href="<?php echo URL; ?>" style="color: #daa520"><img src='<?php echo URL; ?>/images/logo_short.png' height="50px"></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span> </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent1">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item"> <a class="nav-link <?php if($p1==true){echo'active';}?>" href="<?php echo URL; ?>">Главная <span class="sr-only">(current)</span></a> </li>
		<li class="nav-item"> <a class="nav-link <?php if($p2==true){echo'active';}?>" href="<?php echo URL; ?>/about">О нас</a> </li>
		<li class="nav-item"> <a class="nav-link <?php if($p3==true){echo'active';}?>" href="<?php echo URL; ?>/menu">Меню</a> </li>
		<li class="nav-item"> <a class="nav-link <?php if($p4==true){echo'active';}?>" href="<?php echo URL; ?>/points">Кофейни</a> </li>
		<li class="nav-item"> <a class="nav-link <?php if($p5==true){echo'active';}?>" href="<?php echo URL; ?>/contacts">Контакты</a> </li>
    </ul>
	<ul class="navbar-nav justify-content-end">
			<?php ($user) ? include_once($_SERVER['DOCUMENT_ROOT'].'/navAuth/isAuth.php') : include_once($_SERVER['DOCUMENT_ROOT']. '/navAuth/notAuth.php'); ?>
	</ul>
  </div>
</nav>
