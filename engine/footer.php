<footer class="bg-dark text-center text-lg-start text-white ">
  <div class="container p-4">
    <div class="row">
      <div class="col-lg-6">
		  <div class="text-center" style="color:#ffd700">
			  <img src="<?php echo URL; ?>/images/logo_full.png" height="120px">
		  </div>
      </div>

      <div class="col-lg-3">
        <h5 class="text-uppercase">Контакты</h5>
        <ul class="list-unstyled mb-0">
          <li>
            Телефон: +7 (000) 000-00-00
          </li>
          <li>
            E-Mail: support@goldch.ru
          </li>
          <li>
            Основной офис: г. Владивосток, ул. Светланская, 11
          </li>
        </ul>
      </div>

      <div class="col-lg-3">
        <h5 class="text-uppercase mb-0">Соцсети</h5>
		  <ul class="list-unstyled mb-0">
          <li>
            <a href="http://vk.com" class="text-white"><i class="fab fa-vk"></i></a>
          </li>
          <li>
            <a href="http://instagram.com" class="text-white"><i class="fab fa-instagram"></i></a>
          </li>
          <li>
            <a href="http://facebook.com" class="text-white"><i class="fab fa-facebook-f"></i></a>
          </li>
        </ul>
		</div>
    </div>
  </div>

  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © GOLD Coffee House. 2021
  </div>
</footer>