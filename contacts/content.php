<h3 class='display-3 text-center text-light'>Как с нами связаться</h3>
<div class='container lead text-light'>
	<div class='row'>
		<div class='col-6 text-right'>
			Наша электронная почта:
		</div>
		<div class='col-6'>
			support@goldch.ru
		</div>
	</div>
	<div class='row'>
		<div class='col-6 text-right'>
			Менеджер сети:<br>Дорофеева Татьяна Григорьевна
		</div>
		<div class='col-6'>
			+7 (000)-000-00-00
		</div>
	</div>
	<div class='row'>
		<div class='col-6 text-right'>
			Менеджерр кофейни на Светланской:<br>Прилепкин Максим Николаевич
		</div>
		<div class='col-6'>
			+7 (000)-000-00-00
		</div>
	</div>
	<div class='row'>
		<div class='col-6 text-right'>
			Менеджер кофейни на проспекте Красного Знамени:<br>Заморская Александра Юрьевна
		</div>
		<div class='col-6'>
			+7 (000)-000-00-00
		</div>
	</div>
	<div class='row'>
		<div class='col-6 text-right'>
			Менеджер кофейни на Спортивной набережной:<br>Вологдина Ирина Викторовна
		</div>
		<div class='col-6'>
			+7 (000)-000-00-00
		</div>
	</div>
	<div class='row'>
		<div class='col-6 text-right'>
			Менеджер кофейни на Фадеева:<br>Касатонова Елена Игоревна
		</div>
		<div class='col-6'>
			+7 (000)-000-00-00
		</div>
	</div>
</div>