<?php if($user){header('Location: ' . URL . '/profile'); exit;}?>
<div style='height: 80vh; background-color: #0009; padding-top: 5%'>
	<form id="reg_form" method="POST" style="text-align: center;" class="text-light">
		<div class='container text-center'>
			<div class='row justify-content-center'>
				<h3 class="display-3">Регистрация</h3>
			</div>
			<div class='row'>
				<div class='col-4'></div>
				<div class='col-4'>
					<div class='lead' style='background-color: #fff8; height: 10vh'>
						<?php
							if ( isset($_POST['reg']) )
							{
								$login = mysqli_real_escape_string($db, @$_POST['login']);
								$password = mysqli_real_escape_string($db, md5(@$_POST['password']));
								$access = mysqli_real_escape_string($db, @$_POST['access']);

								$regCheck = "SELECT login FROM accounts WHERE login = '$login'";
								$getValue = mysqli_query($db, $regCheck);

								if ( mysqli_num_rows($getValue) > 0 ) { echo '<span style="color: RED; ">Логин занят!</span>'; }
								elseif( $_POST['login'] == '' ) { echo '<span style="color: RED; ">Введите Логин!</span><br>'; }
								elseif( $_POST['password'] == '' ) { echo '<span style="color: RED; ">Введите Пароль!</span><br>'; }
								elseif( !preg_match("/^[a-zA-Z0-9]+$/", $_POST['login']) ) { echo '<span style="color: blue; ">Логин может состоять только из букв латинского алфавита и цифр!</span><br>'; }
								elseif( strlen($_POST['password']) < 5 or strlen($_POST['password']) > 64 ) { echo '<span style="color: blue; ">Пароль должен быть не меньше 5-х символов и не больше 64.</span>'; }

								else {
									if (!mysqli_query($db, "INSERT INTO accounts (login,password,access) VALUES ('$login','$password',0) ")) {
										header('Refresh: 10');
										echo 'Произошла какая-то ошибка. <s>Страница обновится через 10 секунд</s>';
									} else {
										if (isset($login, $password)) {
											if ($result = !authentication($login, $password)) {
												echo '<span style="color: RED; ">Произошла неизвестная ошибка</span>';
											} else {
												$_SESSION = array(
													'login' => $login,
													'password' => $password
												);
												header('Location: ' . URL);
												exit;
											}
										}
									}
								}
							}
?>
					</div>
				</div>
				<div class='col-4'></div>
			</div>
			<div class='row'>
				<div class='col-4'></div>
				<div class='col-4'>
					<div class="form-group">
					  	<label for="login_form"> Логин:
						<input class="form-control text-monospace" type="text" name="login" placeholder="Введите ваш логин" id="login_form" required/>
						</label>
				  	</div>
					<div class="form-group">
					 	<label for="password_form"> Пароль:
						<input class="form-control text-monospace"type="password" name="password" placeholder="Введите ваш пароль" id="password_form" required/>
						</label>
					</div>
					<button type="submit" name="reg" class="btn btn-dark">Зарегистрироваться</button>
					<button type="reset" class="btn btn-dark">Очистить</button>
				</div>
				<div class='col-4'></div>
			</div>
		</div>
	</form>
  </div>