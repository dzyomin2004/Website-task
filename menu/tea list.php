<?php
global $db;
$query_tea = 'SELECT * FROM list_tea INNER JOIN list_tea_volumes USING(id)';
$response_tea =  mysqli_query($db, $query_tea);

$temp_current_tea = NULL;
$temp_previous_tea = NULL;
$list_tea = array();
while($row = mysqli_fetch_array($response_tea)){
	$temp_current_tea = $row['id'];
	if($temp_current_tea == $temp_previous_tea){
		array_push($list_tea[$temp_current_tea]['volume'],$row['volume']);
		array_push($list_tea[$temp_current_tea]['price'],$row['price']);
	}
	else{
		$list_tea[$temp_current_tea] = array(
			'name' => $row['name'],
			'description' => $row['description'],
			'type' => $row['type'],
			'volume' => array(),
			'price' => array()
		);
		array_push($list_tea[$temp_current_tea]['volume'],$row['volume']);
		array_push($list_tea[$temp_current_tea]['price'],$row['price']);
		$temp_previous_tea = $temp_current_tea;
	}
}
?>