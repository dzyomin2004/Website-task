<?php
global $db;
$query = 'SELECT * FROM list_coffee INNER JOIN list_coffee_volumes USING(id)';
$response =  mysqli_query($db, $query);

$temp_current = NULL;
$temp_previous = NULL;
$list_coffee = array();
while($row = mysqli_fetch_array($response)){
	$temp_current = $row['id'];
	if($temp_current == $temp_previous){
		array_push($list_coffee[$temp_current]['volume'],$row['volume']);
		array_push($list_coffee[$temp_current]['price'],$row['price']);
	}
	else{
		$list_coffee[$temp_current] = array(
			'name' => $row['name'],
			'description' => $row['description'],
			'type' => $row['type'],
			'volume' => array(),
			'price' => array()
		);
		array_push($list_coffee[$temp_current]['volume'],$row['volume']);
		array_push($list_coffee[$temp_current]['price'],$row['price']);
		$temp_previous = $temp_current;
	}
}
?>