<?php require_once('coffee list.php');
require_once('tea list.php')?>
<div class="text-center text-light" id="coffee">
	<h1>Кофейные напитки</h1>
	<div class="container text-dark">
		<div class="row row-cols-5">
			<?php
			for($i=1;$i<count($list_coffee)+1;$i++){
				echo(
					'<div class="col custom_col">
						<div class="card custom_menu">
							<img src="'.URL.'/images/menu/coffee/id_'.$i.'.png" class="card-img-top">
							<div class="card-body" style="padding-left: 0; padding-right: 0">
								<h5 class="card-title">'.$list_coffee[$i]['name'].'</h5>
								<p class="card-text" style="font-size: 0.8rem">'.$list_coffee[$i]['description'].'</p>
								<ul class="list-group list-group-flush">
									<li class="list-group-item lead" style="background-color: #0000">'.(($list_coffee[$i]['type']==0)?'Классический':'Авторский').'</li>
									<li class="list-group-item" style="background-color: #0000">'.$list_coffee[$i]['volume'][0].'мл: '.$list_coffee[$i]['price'][0].'&#8381;</li>');
									if(isset($list_coffee[$i]['volume'][1]) and isset($list_coffee[$i]['price'][1])){echo '<li class="list-group-item" style="background-color: #0000">'.$list_coffee[$i]['volume'][1].'мл: '.$list_coffee[$i]['price'][1].'&#8381;</li>';}
									if(isset($list_coffee[$i]['volume'][2]) and isset($list_coffee[$i]['price'][2])){echo '<li class="list-group-item" style="background-color: #0000">'.$list_coffee[$i]['volume'][2].'мл: '.$list_coffee[$i]['price'][2].'&#8381;</li>';}
								echo '</ul></div></div></div>';
			}
			?>
		</div>
	</div>
</div>
<div class="text-center text-light" id="tea">
	<h1>Чайные напитки</h1>
	<div class="container text-dark">
		<div class="row row-cols-5">
			<?php
			for($i=1;$i<count($list_tea)+1;$i++){
				echo(
					'<div class="col custom_col">
						<div class="card custom_menu">
							<img src="'.URL.'/images/menu/tea/id_'.$i.'.png" class="card-img-top">
							<div class="card-body" style="padding-left: 0; padding-right: 0">
								<h5 class="card-title">'.$list_tea[$i]['name'].'</h5>
								<p class="card-text" style="font-size: 0.8rem">'.$list_tea[$i]['description'].'</p>
								<ul class="list-group list-group-flush">
									<li class="list-group-item lead" style="background-color: #0000">'.(($list_tea[$i]['type']==0)?'Классический':'Матча').'</li>
									<li class="list-group-item" style="background-color: #0000">'.$list_tea[$i]['volume'][0].'мл: '.$list_tea[$i]['price'][0].'&#8381;</li>');
									if(isset($list_tea[$i]['volume'][1]) and isset($list_tea[$i]['price'][1])){echo '<li class="list-group-item" style="background-color: #0000">'.$list_tea[$i]['volume'][1].'мл: '.$list_tea[$i]['price'][1].'&#8381;</li>';}
									if(isset($list_tea[$i]['volume'][2]) and isset($list_tea[$i]['price'][2])){echo '<li class="list-group-item" style="background-color: #0000">'.$list_tea[$i]['volume'][2].'мл: '.$list_tea[$i]['price'][2].'&#8381;</li>';}
								echo '</ul></div></div></div>';
			}
			?>
		</div>
	</div>
</div>