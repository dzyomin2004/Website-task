<div id="edit-passwd" style='margin-bottom: 10%; padding-top:10%'>
    <form method="POST">
        <div class='container text-center text-light'>
			<div class='row'>
				<div class='col-4'></div>
				<div class='col-4'>
					<div class='lead' style='background-color: #fff8; height: 5vh'>
						<?php
$id = intval($user['id']);
$password_old_db = htmlspecialchars($user['password']);

if (isset($_POST['edit-passwd']))
{
	$password_old_user = md5($_POST['password_old']);
    $password_new = mysqli_real_escape_string($db, md5($_POST['password_new']));

	if ($password_old_db == $password_old_user){
		if ($password_old_user == $password_new) {
			echo '<span><b>Ошибка.</b> Пароли не могут совпадать</span>';
		}
		else {
			if (mysqli_query($db,"UPDATE accounts SET password='$password_new' WHERE id = '$id';"))
			{
				header("Refresh: 3; ../../../");
				echo '<span>Операция выполнена успешно!</span>';
			}
			else
			{
				header('Refresh: 10');
				echo '<span><b>Ошибка.</b> Изменения не были сохранены. Страница обновится через 10 секунд.</span>';
			}
		}
	}
	else
	{
		echo '<div class="alert alert-danger" style="margin: 10px; width: 30%; text-align: center"><b>Ошибка.</b> Неверный старый пароль.</div>';
	}
}
?>
					</div>
				</div>
				<div class='col-4'></div>
			</div>
			<div class='row'>
				<div class='col'><h3 class='text-monospace'>Редактирование пароля к аккаунту с ID: <span style='color:red'><?php echo $id; ?></span></h3></div>
			</div>
		
			<div class='row'>
				<div class='col-4'></div><div class='col-4'>
				<div class='form-group'>
						<label class='form-label' for="password_input_old">Введите старый пароль: </label>
						<input class='form-control text-monospace' name="password_old" type="text" id="password_input_old" required>
				</div>
				<div class='form-group'>
					<label for="password_input_new">Введите новый пароль: </label>
					<input class='form-control text-monospace' name="password_new" type="text" id="password_input_new" required>
				</div>
					<button type="submit" name="edit-passwd" class="btn btn-dark">Сохранить</button>
					<a href="../" class="btn btn-dark">Назад</a>
				</div><div class='col-4'></div>
			</div>
		</div>
    </form>
</div>
