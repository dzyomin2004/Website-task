<div class='container'>
	<div class='row'>
		<div class='col-3'>
			<div class="list-group">
				<p class='lead text-light list-group-item' style="background-color: #4445">Ваш логин: <?php echo htmlspecialchars($user['login']); ?></p>
				<p class='lead list-group-item' style="background-color: #4445"><span class='text-light'>Уровень ваших прав:</span>  <?php echo access($user['access']);?></p>
				<a href="/profile/user/edit/" class="list-group-item list-group-item-action text-light" style="background-color: #4445">Сменить логин</a>
				<a href="/profile/user/edit pwd/" class="list-group-item list-group-item-action text-light" style="background-color: #4445">Редактировать пароль</a>
			</div>
		</div>
		<div class='col-9'></div>
	</div>
</div>