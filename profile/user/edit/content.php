<div id="edit" style='margin-bottom: 10%; padding-top:10%'>
    <form method="post">
		<div class='container'>
			<div class='row'>
				<div class='col-4'></div>
				<div class='col-4'>
					<div class='lead' style='background-color: #fff8; height: 5vh'>
						<?php
$user_id = intval($user['id']);

if (isset($_POST['edit']))
{
    $login = mysqli_real_escape_string($db, $_POST['login']);

    if (mysqli_query($db,"UPDATE accounts SET login='$login' WHERE id = '$user_id';"))
    {
        header("Refresh: 3; ../../../");
        echo '<span>Операция выполнена успешно!<span>';
    }
    else
    {
        header('Refresh: 10');
        echo 'Ошибка. Изменения не были сохранены. Страница обновится через 10 секунд.';
    }
}
?>
					</div>
				</div>
				<div class='col-4'></div>
			</div>
			<div class='row'><div class='col text-center text-light'>
			<h3 class='text-monospace'>Смена логина пользователя с ID: <span style='color:red'><?php echo $user_id; ?></span></h3><br>
			</div></div>
			<div class='row'>
			<div class='col-4'></div>
			<div class='col-4 text-center text-light'>
				<div class='form-group'>
					<input class='form-control text-monospace' name="login" type="text" id="login_input" placeholder="Введите логин" value="<?php echo htmlspecialchars($user['login']); ?>" required>
				</div>
				<button type="submit" name="edit" class="btn btn-dark">Сохранить</button>
				<a href='../' class='btn btn-dark'>Назад</a>
			</div>
		</div></div>
    </form>
</div>