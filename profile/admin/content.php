<div id="admin_profile" style='margin-bottom:10%'>
  <div class='container'>
	  <div class="row">
		<div class="col-3">
			<div class="list-group">
				<p class='lead text-light list-group-item' style="background-color: #4445">Ваш логин: <?php echo htmlspecialchars($user['login']); ?><br>Ваш ID: <?php echo htmlspecialchars($user['id']); ?></p>
				<p class='lead list-group-item' style="background-color: #4445"><span class='text-light'>Уровень ваших прав:</span>  <?php echo access($user['access']);?></p>
				<a href="/profile/user/edit/" class="list-group-item list-group-item-action text-light" style="background-color: #4445" >Сменить логин</a>
				<a href="/profile/user/edit pwd/" class="list-group-item list-group-item-action text-light" style="background-color: #4445">Редактировать пароль</a>
			</div>
		</div>
		<div class="col-6">

		</div>
        <div class="col-3">
          <div class="list-group">
            <p class="lead text-light list-group-item" style="background-color: #4445">Утилиты:</p>
            <a href="/service/utilities/userlist" class="text-light list-group-item list-group-item-action" style="background-color: #4445">Список пользователей</a>
            <a href="/service/utilities/menu" class="text-light list-group-item list-group-item-action" style="background-color: #4445">Меню</a>
            <a href="/service/utilities/phpMyAdmin" class="text-light list-group-item list-group-item-action" style="background-color: #4445">phpMyAdmin</a>
            </div>
        </div>
	  </div>
	</div>
</div>