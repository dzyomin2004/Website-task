<?php require_once '../engine/config.php'?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <?php require_once '../engine/head.php'?>
    <title>Авторизация</title>
</head>
<body style="background-image: url(<?php echo URL; ?>/images/reg_background.jpg)">
<?php
include_once '../engine/navbar.php';
require_once 'content.php';
include_once '../engine/footer.php'
?>
</body>
</html>
