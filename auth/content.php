<div style='height: 80vh; background-color: #0009; padding-top: 5%'>
	<form id="rauth_block" method="POST" style="text-align: center;" class="text-light">
		<div class='container text-center'>
			<div class='row justify-content-center'>
				<h3 class="display-3">Авторизация</h3>
			</div>
			<div class='row'>
				<div class='col-4'></div>
				<div class='col-4'>
					<div class='lead' style='background-color: #fff8; height: 5vh'>
						<?php
							$login = @$_POST['login'];
							$password = md5(@$_POST['password']);
							if ( !$user ) {
								if ( isset($login, $password) ) {
									if ( $result = !authentication($login, $password) ) {
										echo '<span style="color: #d00; ">Данные введены не верно</span><br>';
									} else {
										$_SESSION = array(
											'login' => $login,
											'password' => $password
										);
										header('Location: ' . URL . '/');
										exit;
									}
								}
							}
							else {
								header('Location: ' . URL);
								exit;
							}
        				?>
					</div>
				</div>
				<div class='col-4'></div>
			</div>
			<div class='row'>
				<div class='col-4'></div>
				<div class='col-4'>
					<div class="form-group">
					  	<label for="login_input"> Логин:
						<input class="form-control text-monospace" type="text" name="login" placeholder="Введите ваш логин" id="login_input" required/>
						</label>
					</div>
					<div class="form-group">
					  	<label for="password_input"> Пароль:
						<input class="form-control text-monospace"type="password" name="password" placeholder="Введите ваш пароль" id="password_input" required/>
						</label>
					</div>
					<button type="submit" class="btn btn-dark">Войти</button>
				</div>
				<div class='col-4'></div>
			</div>
		</div>
	</form>
  </div>