<div class='lead text-center' style='margin-top:1%;margin-bottom: -2%'>
</div>
<div id="edit-bloc" style='margin-bottom: 10%; padding-top:10%'>
	<form method="POST">
		<div class='container text-center text-light'>
			<div class='row'>
				<div class='col-4'></div>
				<div class='col-4'>
					<div class='lead' style='background-color: #fff8; height: 5vh'>
						<?php
						$id = $_GET['id'];

						if ($query_2 = mysqli_query($db,"SELECT * FROM `accounts` WHERE `id`='" . $id . "'"))
							$assoc_2 = mysqli_fetch_assoc($query_2);
						{
							if (isset($_POST['editProfile']))
							{
								$login = mysqli_real_escape_string($db, $_POST['login']);
								$password = mysqli_real_escape_string($db, md5($_POST['password']));
								$access = mysqli_real_escape_string($db, $_POST['access']);

								if (mysqli_query($db,"UPDATE accounts SET login='$login',password='$password',access='$access' WHERE id = '$id';"))
								{
									header("Refresh: 3; ../");
									echo '<span style="color:green; margin-bottom:-2%">Операция выполнена успешно!</span>';
								}
								else
								{
									header('Refresh: 10');
									echo '<span style="color:red; margin-bottom:-2%">Ошибка. Изменения не были сохранены. Страница обновится через 10 секунд.</span>';
								}
							}
							if (isset($_POST['deleteProfile']))
							{
								if (mysqli_query($db,"DELETE FROM `accounts` WHERE `accounts`.`id` = '$id';"))
								{
									header("Refresh: 3; ../");
									echo '<span style="color:green; margin-bottom:-2%">Операция выполнена успешно!</span>';
								}
								else
								{
									header('Refresh: 10');
									echo '<span style="color:red; margin-bottom:-2%">Ошибка. Изменения не были сохранены. Страница обновится через 10 секунд.</span>';
								}
							}
						}
?>
					</div>
				</div>
				<div class='col-4'></div>
			</div>
			<div class='row'><div class='col-4'></div>
				<div class='col-4'>
					<div class='form-group'>
						<label for="login_input">Логин</label>
						<input class='form-control text-monospace' name="login" type="text" id="login_input" placeholder="Введите логин" value="<?php echo htmlspecialchars($assoc_2['login']); ?>" required>
					</div>
					<div class='form-group'>
						<label for="password_input">Пароль</label>
						<input class='form-control text-monospace' name="password" type="text" id="password_input" placeholder="Введите пароль" value="<?php echo htmlspecialchars($assoc_2['password']); ?>" required>
					</div>
					<div class='form-group'>
						<label for="access_input">Уровень доступа</label>
						<select class='form-control text-monospace' name="access" id="access_input">
							<option disabled value="<?php echo intval($assoc_2['access']); ?>" style='color:red'>Текущий: <?php echo access($assoc_2['access']);?></option>
            				<option value="0">Пользователь</option>
            				<option value="1">Администратор</option>
						</select>
					</div>
					<button type="submit" name="editProfile" class="btn btn-dark">Сохранить</button>
					<button type='submit' name="deleteProfile" class="btn btn-dark">Удалить пользователя</button>
				</div>
			<div class='col-4'></div></div>
		</div>
	</form>
</div>