<div class="bloc bloc-fill-screen l-bloc pt-5" id="newUser">
	<h3 class="display-3 text-center text-light">Создание нового пользователя</h3>
    <div class="container text-center text-light">
		<div class="row">
			<div class='col-4'></div>
			<div class='col-4'><div class='lead' style='background-color: #fff8;'>
					<?php
				if ( isset($_POST['newUser']) ){
					$login = mysqli_real_escape_string($db, $_POST['login']);
					$password = mysqli_real_escape_string($db, md5($_POST['password']));
					$access = mysqli_real_escape_string($db, $_POST['access']);
					$regCheck = "SELECT login FROM accounts WHERE login = '$login'";
					$getValue = mysqli_query($db, $regCheck);

					if ( mysqli_num_rows($getValue) > 0 ) {echo '<span style="color: #ff0000; ">Логин занят!</span>';}
					else {
					if (!mysqli_query($db, "INSERT INTO accounts (login,password,access) VALUES ('$login','$password','$access') ")){
						header('Refresh: 10');
						echo '<span style="color: RED; ">Произошла какая-то ошибка. Страница обновится через 10 секунд</span>';
					}
					else
						echo '<span style="color: green; ">Успех!</span>';
					}
					}
			?>
				</div>
			</div>
			<div class='col-4'></div>
		</div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 offset-md-2 col-md-8">
                <form method="POST">
                    <div class="form-group">
                        <label for="login_input">Логин</label>
                        <input name="login" id="login_input" class="form-control text-monospace" placeholder="Введите логин" maxlength="32" required/>
                    </div>

                    <div class="form-group">

                        <label for="password_input">Пароль</label>
                        <input name="password" type="text" id="password_input" placeholder="Введите пароль" maxlength="64" class="form-control text-monospace" required>

                        <div class="form-group">
                            <label for="access_input">Доступ</label>
                            <select class="form-control option-select-margin-top text-monospace" id="access_input" name="access">
                                <option value="0">Пользователь</option>
                                <option value="1">Администратор</option>
                            </select>
                        </div>

                    </div>
                    <button class="bloc-button btn btn-dark float-lg-none" name="newUser" type="submit">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>