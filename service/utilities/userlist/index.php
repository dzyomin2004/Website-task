<?php require_once '../../../engine/config.php'?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <?php require_once '../../../engine/head.php'?>
    <title>Список пользователей</title>
</head>
<body>
<?php
include_once '../../../engine/navbar.php';
require_once 'content.php';
include_once '../../../engine/footer.php'
?>
</body>
</html>
