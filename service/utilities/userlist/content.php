<div>
	<h3 class="text-center text-light display-3">Список пользователей сайта:</h3>
	<div class='text-center' style='margin-bottom: 1%'><a class="btn btn-dark btn-lg" href="create user/" role="button">Добавить нового пользователя</a></div>
	<div class="list-group bg-dark text-light">
		<?php
		$query_1 = mysqli_query($db,"SELECT * FROM accounts");
		while ( $assoc_1 = mysqli_fetch_assoc($query_1) ) {
			echo '<a class="list-group-item list-group-item-action text-light bg-dark lead"href="edit/?id='.intval($assoc_1['id']).'">' . htmlspecialchars($assoc_1['login']) . '</a>';
		}
		?>
	</div>
</div>