<div>
	<h3 class="text-center text-light display-3">Меню:</h3>
	<div class='text-center' style='margin-bottom: 1%'>
		<a class="btn btn-dark btn-lg" href="addCoffee/" role="button">Добавить новый кофейный напиток</a>
		<a class="btn btn-dark btn-lg" href="addTea/" role="button">Добавить новый чайный напиток</a>
	</div>
	<ul class="list-group bg-dark text-light">
		<li class="list-group-item bg-dark text-center lead">Кофейные напитки</li>
		<?php
		$query_menu_list = mysqli_query($db,"SELECT * FROM list_coffee");
		while ( $row_menu_list = mysqli_fetch_assoc($query_menu_list) ) {
			echo '<li class="list-group-item bg-dark"><span class="lead">'.$row_menu_list['name'].'</span></li>';
		}
		?>
		<li class="list-group-item bg-dark text-center lead">Чайные напитки</li>
		<?php
		$query_menu_list = mysqli_query($db,"SELECT * FROM list_tea");
		while ( $row_menu_list = mysqli_fetch_assoc($query_menu_list) ) {
			echo '<li class="list-group-item bg-dark"><span class="lead">'.$row_menu_list['name'].'</span></li>';
		}
		?>
	</ul>
</div>