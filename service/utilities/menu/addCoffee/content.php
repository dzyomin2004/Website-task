<div class="bloc bloc-fill-screen l-bloc" id="addCoffeeList">
    <div class="container">
		<div class='row'>
			<div class='col-4'></div>
			<div class='col-4 text-center align-middle'>
				<div class='lead' style='background-color: #fff8; height: 10vh'>
						<?php
	if(isset($_POST['addCoffeeList'])){
		global $db;
		$name = mysqli_real_escape_string($db, $_POST['name']);
		$description = mysqli_real_escape_string($db, $_POST['description']);
		$type = mysqli_real_escape_string($db, $_POST['type']);
		$id = (mysqli_num_rows(mysqli_query($db,'SELECT * FROM list_coffee'))+1);
		
		$volume = array(
			mysqli_real_escape_string($db, $_POST['volume1']),
			mysqli_real_escape_string($db, $_POST['volume2']),
			mysqli_real_escape_string($db, $_POST['volume3'])
		);
		$price = array(
			mysqli_real_escape_string($db, $_POST['price1']),
			mysqli_real_escape_string($db, $_POST['price2']),
			mysqli_real_escape_string($db, $_POST['price3'])
			);
		if(!mysqli_query($db,"INSERT INTO list_coffee (name, description, type) VALUES ('$name','$description','$type')")){
			header('Refresh: 10');
			echo '<span style="color: RED; ">Произошла какая-то ошибка при добавлении напитка. Страница обновится через 10 секунд</span>';
		}
		else{
			for($i=0;$i<3;$i++){
				if($volume[$i] == !NULL and $price[$i] == !NULL){
					if(!mysqli_query($db,"INSERT INTO list_coffee_volumes (id, volume, price) VALUES ('$id','$volume[$i]',$price[$i])")){
						echo '<span style="color: RED; ">Произошла какая-то ошибка при добавлении объёмов. Страница обновится через 10 секунд</span>';
						exit;
					}
				}
			}
			echo '<p style="color: green">Пункт меню успешно добавлен!</p>';
		}
	}
?>
				</div>
			</div>
		</div>
        <div class="row text-light text-center">
            <div class="col-lg-6 offset-lg-3 offset-md-2 col-md-8">
                <form method="POST">
                    <h3 class="mg-md tc-black h3-style">Добавление кофейного напитка</strong></h3>
                    <div class="form-group">
                        <label for="name_input">Название</label>
                        <input name="name" id="name_input" class="form-control" placeholder="Название" maxlength="64" required/>
                    </div>
                    <div class="form-group">
                        <label for="description_input">Описание</label>
						<textarea name="description" id="description_input" placeholder="Описание" maxlength="1024" class="form-control" required></textarea>
               		<div class="form-group">
                            <label for="access_input">Тип</label>
                            <select class="form-control option-select-margin-top" id="type_input" name="type">
                                <option value="0">Классический</option>
                                <option value="1">Авторский</option>
                            </select>
                        </div>
                    </div>
				<div class="from-group">
					<label for="description_input">Объём 1</label>
					<input name="volume1" id="volume1_input" class="form-control" placeholder="Объём" maxlength="64"required/>
					<input name="price1" id="price1_input" class="form-control" placeholder="Цена" maxlength="64"required/>
					<label for="description_input">Объём 2</label>
					<input name="volume2" id="volume2_input" class="form-control" placeholder="Обьъём" maxlength="64" />
					<input name="price2" id="price2_input" class="form-control" placeholder="Цена" maxlength="64"/>
					<label for="description_input">Объём 3</label>
					<input name="volume3" id="volume3_input" class="form-control" placeholder="Объём" maxlength="64"/>
					<input name="price3" id="price3_input" class="form-control" placeholder="Цена" maxlength="64"/>
				</div>
                    <button class="btn btn-dark mt-1 mb-1" name="addCoffeeList" type="submit">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>