<?php 
$col = 'class="list-group-item" style="background-color: #0004"'
?>
<div class='container'>
	<div class='row'>
		<div class='col text-center'>
			<h1 class='display-1 text-light'>Кофейни нашей сети</h1>
		</div>
	</div>
	<div class='row text-center text-light'>
		<div class='col-2' style="background-color: #0004">
			<p>1. Свтеланская 11<br><span class='lead'>Центр</span></p>
		</div>
		<div class='col-4' style="background-color: #0004">
			<p>Кофейня, которая появилась первой. Она же впоследсвтии стала главной кофейней всей сети.<br>Здесь создана уютная домашняя атмосфера, поэтому здесь можно собраться как большой весёлой компанией, так и своей небольшой семьёй.<br>Для тех, кто любит уединение для работы также есть свой отдельный уголок.<br>Из большого панорамного окна открывается вид на здание Правительсвта Приморского края и оживлённые улицы центра Владивостока.</p>
		</div>
		<div class='col-6 '><img class='mb-3' src='../images/svetlanskaya.jpg' width='100%'></div>
	</div>
	<div class='row text-center text-light'>
		<div class='col-2' style="background-color: #0004">
			<p>2. Проспект красного знамени 59<br><span class='lead'>ВГУЭС</span></p>
		</div>
		<div class='col-4' style="background-color: #0004">
			<p>Небольшая кофейня, созданная, в первую очередь, для студентов.<br>Здесь можно заняться учёбой, наслаждаясь стаканом кофе, или, наоборот, отдохнуть от неё за чашечкой чая.<br>Для удобства в по периметру мы расположили много розеток, а также свободных USB-разъёмов.<br>А также на территории кофейни доступна скоростная Wi-Fi сеть, пароль для авторизации в которой можно попросить у наших бариста.</p>
		</div>
		<div class='col-6 '><img class='mb-3' src='../images/prospekt.jpg' width='100%'></div>
	</div>
	<div class='row text-center text-light'>
		<div class='col-2' style="background-color: #0004">
			<p>3. Спортивная набережная</p>
		</div>
		<div class='col-4' style="background-color: #0004">
			<p>Компактная кофейня, расположенная в удобном месте.<br>Здесь всегда можно приобрести согревающий напиток на вечер для романтичной прогулки по набережной, или охлаждающий лимонад для летних гуляний под солнцем.</p>
		</div>
		<div class='col-6 '><img class='mb-3' src='../images/neberejnaya.jpg' width='100%'></div>
	</div>
	<div class='row text-center text-light'>
		<div class='col-2' style="background-color: #0004">
			<p>4. Фадеева 2Д<br><span class='lead'>Рынок</span></p>
		</div>
		<div class='col-4' style="background-color: #0004">
			<p>Самая новая из всех кофеен. Ориентирована, в первую очередь, для работы на вынос, так как расположена на территории рынка. Однако здесь вы также можете расположиться свой компанией и с удовольствием провести время.</p>
		</div>
		<div class='col-6 '><img src='../images/fadeeva.jpg' width='100%'></div>
	</div>
	<div class='row justify-content-center'>
		<div class='col-3'></div>
		<div class='col-6 mt-3 mb-3'>
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ab23b132d2874f2360ddc095fbf77fa19ccdf631af09d39f6523fcafd979db4f8&amp;width=600&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
		</div>
		<div class='col-3'></div>
	</div>
</div>